## matchmaking-bot

Fork of https://git.sr.ht/~gavodavo/matchmaking-bot to add support for multiple games.

A simple Python bot for IRC channels that want matchmaking capabilities.

## Dependencies

You just need configparser and irctokens. You can install them doing:
```
pip install configparser irctokens
```
You can add `--user` if you don't want to install them system wide.

## Installation

Just `./bot.py` lol.

## Configuration

The bot uses a bot.ini for configurating the servers for connecting. Look at
bot.ini.example for an example(you know, like the extension says).
